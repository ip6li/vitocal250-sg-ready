# Vitocal 250 meets Rasperry PI/Shelly

Das ist jetzt ein erster Wurf für die Implementierung einer "intelligenten" Steuerung
der Wärmepumpe auf Basis der Betriebsdaten einer Photovoltaik Anlage.

Mit SG Ready ist das alles noch sehr grob, evtl kann in Verbindung mit dem
Projekt [CAN Bus, Home Automation E3 Generation lokal](https://www.viessmann-community.com/t5/Konnektivitaet/CAN-Bus-Home-Automation-E3-Generation-lokal-und-kostenlos/td-p/356066) eine feinere Regelung implementiert werden.

# Viessmann Cloud

Bei dieser Anwendung war von Anfang an eine vollständige Unabhängigkeit von der Viessmann Cloud eine Anforderung.
Das Viessmann API ist leider von der Verfügbarkeit nicht dazu geeignet, die Vitocal permanent zu steuern.

Die vorliegende Lösung erfordert lediglich ein stabil funktionierenes LAN, dabei spielen folgende, aktive
Komponenten eine Rolle:

* Ethernet Switch, hier ist ein managebares Produkt vorzuziehen, weil damit eine Überwachung der LAN
Funktionen möglich ist.
* Raspberry PI (oder anderer Server) - dort laufen diese Skripte.
* Wechselrichter mit Smartmeter (hier Fronius Symo Gen24 plus mit Fronius Smartmeter).
* [Shelly Pro 3](https://www.shelly.com/de/products/shop/shelly-pro-3-1) (3fach Relais zur Steuerung von SG Ready an der Vitocal 250).

# Shelly Pro 3

Leider liegen über den potenzialfreien Relais Kontakten 1nF Kondensatoren, die SG Ready empfindlich
stören. Diese müssen aus dem Shelly Pro3 entfernt werden.

**Warnung**: Zum Entfernen der Kondensatoren muss der Shelly Pro3 geöffnet werden. Dort befinden sich
Kontaktflächen, die Netzspannung führen. Ein unsachgemäßes Vorgehen bedeutet hier
**Lebensgefahr**. Im Zweifel diese Arbeiten von einem Fachmann durchführen lassen. Das sind ca. 15 Min. Arbeit.

# Fronius Collector

Unter ./fronius liegt ein Git Submodule für den Fronius Datensammler. In dem Python Skript *collecter.py* müssen
der Hostname und ggf. die url angepasst werden. Das Skript wird dann über den Systemd gestartet.
Das Collector Skript zieht die Daten vom Fronius Wechselrichter über dessen API und legt die Daten in
ein lokalen SQlite3 Datenbank ab.

## Systemd

Es ist die Datei */etc/systemd/system/fronius.service* anzulegen:

```
# systemd configuration for weewx

[Unit]
Description=Fronius Data Collector
Requires=time-sync.target
After=time-sync.target network.target
RequiresMountsFor=/home

[Service]
ExecStart=/home/fronius/fronius-collect/collecter.py
Type=simple
User=fronius
Group=fronius

[Install]
WantedBy=multi-user.target
```

Ggf. müssen die Pfade angepasst werden. Mit *systemctl enable --now* wird der Collector gestartet.

# Leitungsparameter PV Anlage

Das Skript *viessmann-control.sh* wird über einen Cronjob alle 5 Min. gestartet und ermittelt über
eine SQlite3 Abfrage einen Mittelwert der eingespeisten Leitung über 30 Minuten.

In *viessmann-control.sh* müssen die Schaltschwellen in den Variablen PV_SG_MODE4, PV_SG_MODE3 und PV_SG_MODE2
passend gesetzt werden. Das ist je nach Parametriesierung der Vitocal 250 und den Leistungsdaten
der PV Anlage unterschiedlich.

PV_SG_MODE4 sollte nicht verwendet werden, nach den ersten Erfahrungen scheint es auszureichen, bei
genügend Einspeiseleitung PV_SG_MODE3 zu verwenden. Im Skript wurde daher der Wert für Zwangsbetrieb
auf -99999 gesetzt.

## EVU Sperre

*noch nicht implementiert*

# Skript IDs ermitteln

Mit ./shelly.sh list kann man die Skript IDs ermitteln. Diese müssen
in viessmann-control.sh den Betriebszuständen richtig zugeordnet
werden.

```
{
  "id": 1,
  "src": "shellypro3",
  "result": {
    "scripts": [
      {
        "id": 1,
        "name": "Betriebszustand 4",
        "enable": false,
        "running": false
      },
      {
        "id": 2,
        "name": "Betriebszustand 2",
        "enable": false,
        "running": false
      },
      {
        "id": 3,
        "name": "Betriebszustand 1",
        "enable": false,
        "running": false
      },
      {
        "id": 4,
        "name": "Betriebszustand 3",
        "enable": false,
        "running": false
      }
    ]
  }
}
```

# SG Ready

Eine gute Erklärung der mit SG Ready einstellbaren Betriebszustände ist in einem
[Beitrag im Viessmannforum](https://www.viessmann-community.com/t5/Waermepumpe-Hybridsysteme/Vitocal-250-A-und-Smart-Grid/m-p/347817)
zu finden.

## Betriebsart 3

Betrieb der Wärmepumpe mit angepassten Temperatur-Sollwerten für verschiedene Funktionen. 
* Der Verdichter schaltet sich nur bei Bedarf ein. Die gültigen Einschaltbedingungen für die jeweilige Funktion müssen erfüllt sein. Für die jeweilige Funktion muss im Zeitprogramm eine Zeitphase aktiv sein.
* Auf den Heizwasser-Durchlauferhitzer haben die angepassten Temperatur-Sollwerte keinen Einfluss. Der Heizwasser-Durchlauferhitzer wird bei Unterschreitung der Grenzen eingeschaltet, die ohne Smart Grid gelten.

Betrieb der Wärmepumpe mit angepassten Temperatur-Sollwerten für verschiedene Funktionen. Die Änderungen werden mit folgenden Parametern eingestellt:

* Raumbeheizung - Smart Grid Sollwertanhebung für Raumtemperatur Parameter 2543.0
* Raumkühlung - Smart Grid Sollwertanhebung für Raumkühlung Parameter 2543.1
* Trinkwassererwärmung - Smart Grid Sollwertanhebung für Trinkwassererwärmung Parameter 2543.2
* Erwärmung Heizwasser-Pufferspeicher  - Smart Grid Sollwertanhebung für Heizbetrieb Heizwasser-Pufferspeicher Parameter 2543.3 
* Kühlung Heizwasser-Pufferspeicher - Smart Grid Sollwertabsenkung für Kühlbetrieb Heizwasser-Pufferspeicher Parameter 2543.4

## Betriebsart 4

Die Anlagenkomponenten werden auf die eingestellten max. Temperaturen beheizt oder auf die Mindesttemperaturen gekühlt. Der Verdichter schaltet sich sofort ein, auch wenn keine Zeitphase im Zeitprogramm aktiv ist.

Im Modus 4 wirken die folgenden Maximalwerte Heizen, Kühlen und Warmwasser:
* Max. Vorlauftemperatur Heiz-/Kühlkreis 1 Parameter 1192.1
* Max. Vorlauftemperatur Heiz-/Kühlkreis 2 Parameter 1193.1
* Max. Vorlauftemperatur Heiz-/Kühlkreis 3 Parameter 1194.1
* Max. Vorlauftemperatur Heiz-/Kühlkreis 4 Parameter 1195.1
* Max. Warmwassertemperatur Parameter  504.4
* Min. Vorlauftemperatur Kühlung Heiz-/Kühlkreis 1 Parameter  2409.0
* Min. Vorlauftemperatur Kühlung Heiz-/Kühlkreis 2 Parameter 2410.0
* Min. Vorlauftemperatur Kühlung Heiz-/Kühlkreis 3 Parameter 2411.0
* Min. Vorlauftemperatur Kühlung Heiz-/Kühlkreis 4 Parameter 2412.0

