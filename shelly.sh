#!/usr/bin/env bash

usage(){
  echo "usage: $0 [mode1|mode2|mode3|mode4]"
  exit 1
}

shelly_script(){
  url="$1"
  script_id="$2"
  curl --silent -X POST -d "{\"id\":1,\"method\":\"Script.Start\",\"params\":{\"id\":${script_id}}}" "${url}"
}

MODE="$1"

if [ -z "$MODE" ]; then
  usage
fi

PATH=/usr/sbin:/sbin:/usr/bin:/bin
export PATH

SHELLY='shelly-pro3.example.com'

case $MODE in
  mode1) echo "mode 1"
         shelly_script "http://${SHELLY}/rpc" 3 | jq .
         ;;
  mode2) echo "mode 2"
         shelly_script "http://${SHELLY}/rpc" 2 | jq .
         ;;
  mode3) echo "mode 3"
         shelly_script "http://${SHELLY}/rpc" 4 | jq .
         ;;
  mode4) echo "mode 4"
         shelly_script "http://${SHELLY}/rpc" 1 | jq .
         ;;
  list)  echo "List scripts"
         curl --silent -X POST -d '{"id":1,"method":"Script.List"}' "http://${SHELLY}/rpc" | jq .
         ;;
esac

