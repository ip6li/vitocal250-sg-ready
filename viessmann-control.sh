#!/usr/bin/env bash

PATH=/usr/sbin:/sbin:/usr/bin:/bin
export PATH

cd /home/fronius || exit 1

INTERVAL=360
INTERVAL_S=$(echo "${INTERVAL} * 5 / 60" | bc)
PV_SG_MODE4=-99999
PV_SG_MODE3=-1500
PV_SG_MODE2=-1000

# sqlite> select P_GRID from Site order by Timestamp desc limit 1;
# -4621.7
MEAN_PV_POWER=$(sqlite3 /tmp/Fronius.sqlite "SELECT avg(P_GRID) FROM (select P_GRID from Site order by Timestamp desc limit ${INTERVAL});" | cut -d. -f1)

if [ -z "$MEAN_PV_POWER" ]; then
  echo "Fehler in PV Erfassung"
  exit 1
fi

# MEAN_PV_POWER < 0 means PV energy is available
if (( $MEAN_PV_POWER < $PV_SG_MODE4 )); then
  echo "Mode 4 - ${INTERVAL_S}m Mittelwert ${MEAN_PV_POWER}W"
  ./shelly.sh mode4 > /dev/null
elif (( $MEAN_PV_POWER < $PV_SG_MODE3 )); then
  echo "Mode 4 - ${INTERVAL_S}m Mittelwert ${MEAN_PV_POWER}W"
  ./shelly.sh mode3 > /dev/null
else
  echo "Mode 2 - ${INTERVAL_S}m Mittelwert ${MEAN_PV_POWER}W"
  ./shelly.sh mode2 > /dev/null
fi

